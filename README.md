## Avatar Picker

A simple react component for selecting an avatar. It looks like this:

![Pick an avatar](./avatar-picker.gif "Pick me!")

The user clicks on the avatar to open a popover which allows the user to choose another avatar from the list. After making a selection and clicking on an avatar image, a simulated HTTP request is made (while a loading spinner runs), the popover closes, and the new avatar appears in the initial view.

### Installation

* Clone this repo.
* In the project folder, run `npm install` or `yarn` to install all project dependencies.
* To run the demo app, run `npm start` or `yarn start`.

### Usage
* The `AvatarPicker` component takes a single property, `avatars`, which is a list of available avatars. This list is an array of objects, each of which consists of the src of the avatar image file, the avatar label and a unique avatar id. The number of avatars can vary. For example, this demo uses the following `avatars` array:  

```javascript
[
  { "src": "avatar1.png", "label": "Avatar 1", "id": 1 },
  { "src": "avatar2.png", "label": "Avatar 2", "id": 2 },
  { "src": "avatar3.png", "label": "Avatar 3", "id": 3 },
  { "src": "avatar4.png", "label": "Avatar 4", "id": 4 },
  { "src": "avatar5.png", "label": "Avatar 5", "id": 5 },
  { "src": "avatar6.png", "label": "Avatar 6", "id": 6 }
]
```

### Example
```javascript
import React, { Component } from 'react';
import AvatarPicker from './components/AvatarPicker';
import avatars from './data/avatars.js';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AvatarPicker avatars={avatars} />
      </div>
    );
  }
}

export default App;
```

* NOTE: The list of avatars for this demo is imported in from a file containing the array as given in the example above. Your usage may require pulling this data from your database.

Happy coding! :computer:


### Tech Stack:
- [ReactJS](https://github.com/facebook/react/)


### Libraries:
- [Styled components](https://github.com/styled-components/styled-components)
- [React animations](https://github.com/FormidableLabs/react-animations)


### Attributions

* Avatar icons used in this demo by <a href="http://www.freepik.com/free-photos-vectors/people">Freepik</a>

* Built with :heart: for the [Colony coding challenge](https://github.com/JoinColony/coding-challenge).


### License

Submit any bugs by creating an issue or submitting a PR.
This repo is published under the MIT License.
