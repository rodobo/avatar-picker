import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css, keyframes } from 'styled-components';
import Avatar from '../Avatar';

import { AvatarShape } from '../../data/models';

import { merge, fadeIn, pulse } from 'react-animations';

const fadeInPulse = merge(fadeIn, pulse);
const popoverDisplayAnimation = keyframes`${fadeInPulse}`;

const Container = styled.div`
  width: 310px;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 10px;
`;

const PopOverContainer = styled.div`
  transition: all 0.2s;
  ${props => !props.show && css`
    transition-timing: linear;
    opacity: 0;
    transform: scale(0);
  `}
  ${props => props.show && css`
    animation: 0.2s ${popoverDisplayAnimation};
  `}
  &:focus {
    outline: 0;
  }
`;

const Triangle = styled.div`
  width: 0;
  height: 0;
  border-width: 8px;
  border-style: solid;
  border-color: transparent;
  border-bottom: 8px solid rgb(44, 48, 51);
  margin: 0 auto;
`;

const PopOver = styled.div`
  width: 280px;
  border-radius: 2px;
  background-color: rgb(44, 48, 51);
  box-shadow: 2px 2px 10px -2px rgb(102, 102, 102);
  padding: 14px 8px;
`;

const Title = styled.p`
  font-family: Source Sans Pro;
  color: rgb(249, 249, 249);
  font-size: 16px;
  margin: 14px 0;
  text-align: center;
`;

const AvatarList = styled.ul`
  list-style: none;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-content: space-around;
`;

const AvatarListItem = styled.li`
  width: 56px;
  height: 62px;
  margin: 7px;
  display: flex;
  justify-content: center;
`;

export default class AvatarPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: this.props.avatars[0],
      showPopover: false,
    }
  }

  handleClickAvatar = (avatar) => {
    if (!this.state.showPopover) { // add/remove click event listener
      document.addEventListener('click', this.handleClickToClose, false);
    } else {
      document.removeEventListener('click', this.handleClickToClose, false);
    }

    this.setState(prevState => ({
      avatar: avatar ? avatar : prevState.avatar,
      showPopover: !prevState.showPopover,
    }));
  }

  handleClickToClose = (e) => {
    // prevent closing popover if click detected within its boundaries
    // adapted from: https://codepen.io/graubnla/pen/EgdgZm
    if (this.popOverRef.contains(e.target)) return;
    this.handleClickAvatar();
  }

  render() {
    // setup popover ref
    const popOverRef = el => {
      this.popOverRef = el;
    };

    return (
      <Container>
        <Avatar
          avatar={ this.state.avatar }
          isFeatured={ true }
          onClickAvatar={ this.handleClickAvatar.bind(this, null) }
          tabIndex="0"
        />
        <PopOverContainer show={ this.state.showPopover }>
          <Triangle />
          <PopOver>
            <div ref={ popOverRef }>
              <Title>Choose your avatar</Title>
              <AvatarList>
                { this.props.avatars.map(avatar => (
                  <AvatarListItem key={ avatar.id }>
                    <Avatar
                      avatar={ avatar }
                      isSelected={ avatar.id === this.state.avatar.id }
                      onClickAvatar={ this.handleClickAvatar.bind(this, avatar) }
                      tabIndex={ avatar.id }
                    />
                  </AvatarListItem>
                ))}
              </AvatarList>
            </div>
          </PopOver>
        </PopOverContainer>
      </Container>
    );
  }
}

AvatarPicker.propTypes = {
  avatars: PropTypes.arrayOf(AvatarShape),
  show: PropTypes.bool,
};

AvatarPicker.defaultProps = {
  avatars: [],
  show: false,
};
