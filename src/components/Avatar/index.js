import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css, keyframes } from 'styled-components';

import { AvatarShape } from '../../data/models';

const rotatingAnimation = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const AvatarImg = styled.img`
  width: 54px;
  height: 54px;
  border-radius: 35px;
`;

const AvatarButton = styled.button`
  width: 60px;
  height: 60px;
  border-radius: 35px;
  border: 3px solid transparent;
  background-color: transparent;
  padding: 0;
  position: relative;
  &:focus {
    outline: 0;
  }
  ${props => props.isSelected && css`
    border: 3px solid rgb(122, 161, 178);
  `}
  ${props => props.isFeatured && css`
    width: 56px;
    height: 56px;
    border-width: 1px;
  `}
  &:active, &:hover, &:focus {
    cursor: pointer;
    border: 3px solid rgb(155, 160, 163);
    ${props => props.isSelected && css`
      border-color: rgb(122, 161, 178);
      cursor: default;
      opacity: 1;
    `}
    ${props => props.isFeatured && css`
      border-width: 1px;
      opacity: 1;
    `}
  }
`;

const AvatarRotatingBorder = styled.div`
  display: none;
  ${props => props.show && css`
    display: block;
    position: absolute;
    top: -3px;
    left: -3px;
    width: 54px;
    height: 54px;
    border-radius: 35px;
    border-top: 3px solid rgb(122, 161, 178);
    border-bottom: 3px solid rgb(122, 161, 178);
    border-left: 3px solid rgb(122, 161, 178);
    border-right: 3px solid rgb(44, 48, 51);
    animation: 1.5s infinite ${rotatingAnimation};
  `}
`;

const AvatarOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 54px;
  height: 54px;
  border-radius: 35px;
  background-color: rgb(122, 161, 178);
  opacity: 0.2;
  display: none;

  ${props => props.displayOnHover && css`
    ${AvatarButton}:hover & {
      display: block;
    }
  `}
`;

class Avatar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
    }
  }

  handleClick = (onClickAvatar) => {
    if (!this.props.isSelected) { // prevents clicking on selected avatar
      const delay = this.props.isFeatured ? 0 : 1000; // set delay to simulate http request
      this.setClicked(this.delayFunc.bind(null, onClickAvatar, delay));
      // ensures rotating border and animation are hidden after popover closes
      if (!this.props.isFeatured) this.delayFunc(this.setClicked, 1200);
    }
  }

  setClicked = (cb) => {
    this.setState(prevState => ({
      clicked: !prevState.clicked,
    }), cb);
  }

  delayFunc = (func, delay) => {
    window.setTimeout(func, delay);
  }

  render = () => {
    return (
      <AvatarButton
        image={ `./images/${this.props.avatar.src}` }
        onClick={ this.handleClick.bind(null, this.props.onClickAvatar) }
        isSelected={ this.props.isSelected }
        isFeatured={ this.props.isFeatured }
      >
        <AvatarImg src={ `./images/${this.props.avatar.src}` } />
        <AvatarOverlay
          displayOnHover={ !this.props.isSelected && !this.props.isFeatured }
        />
        <AvatarRotatingBorder show={ !this.props.isFeatured && this.state.clicked } />
      </AvatarButton>
    );
  }
};

Avatar.propTypes = {
  avatar: AvatarShape,
  onClickAvatar: PropTypes.func,
  isSelected: PropTypes.bool,
  isFeatured: PropTypes.bool,
  show: PropTypes.bool,
  displayOnHover: PropTypes.bool,
};

Avatar.defaultProps = {
  avatar: {},
  onClickAvatar: undefined,
  isSelected: false,
  isFeatured: false,
  show: false,
  displayOnHover: false,
};

export default Avatar;
