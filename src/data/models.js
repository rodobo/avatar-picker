import PropTypes from 'prop-types';

export const AvatarShape = PropTypes.shape({
  src: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.number,
});
