import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AvatarPicker from './components/AvatarPicker';
import avatars from './data/avatars';

import { AvatarShape } from './data/models';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AvatarPicker avatars={ avatars } />
      </div>
    );
  }
}

App.propTypes = {
  avatars: PropTypes.arrayOf(AvatarShape),
};

App.defaultProps = {
  avatars: [],
};

export default App;
